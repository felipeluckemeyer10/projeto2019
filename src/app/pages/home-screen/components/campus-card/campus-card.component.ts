import {Component, Input} from '@angular/core';
import {Campus} from '../../models/campus.model';

@Component({
  selector: 'app-campus-card',
  templateUrl: './campus-card.component.html',
  styleUrls: ['./campus-card.component.scss']
})
export class CampusCardComponent {
  pages: {url: string; icon: string; text: string}[];

  initializeApp() {
    this.pages = [{url: 'tasks', icon: 'checkmark', text: 'Tasks'}];
  }
  @Input() campus: Campus;
}
