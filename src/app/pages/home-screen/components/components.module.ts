import { NgModule } from '@angular/core';
import { CampusCardComponent } from './campus-card/campus-card.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CampusCardComponent],
  imports: [
    SharedModule
  ],
  exports: [
    CampusCardComponent
  ]
})
export class ComponentsModule { }
