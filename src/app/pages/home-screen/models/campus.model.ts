export interface Campus {
  id: string;
  nome: string;
  endereco: string;
  telefone: string;
}
