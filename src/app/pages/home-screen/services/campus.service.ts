import { Injectable } from '@angular/core';
import { Firestore } from 'src/app/core/classes/firestore.class';
import { Campus } from '../models/campus.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CampusService extends Firestore<Campus> {

  constructor(private authService: AuthService, db: AngularFirestore) {
    super(db);
    this.init();
   }

   private init(): void {
     this.authService.authState$.subscribe(user => {
       if (user) {
         this.setCollection(`/campus`);
         return;
       }
       this.setCollection(null);
     });
   }

}
