import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusDadosPage } from './campus-dados.page';

describe('CampusDadosPage', () => {
  let component: CampusDadosPage;
  let fixture: ComponentFixture<CampusDadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusDadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusDadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
