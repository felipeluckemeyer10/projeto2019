// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAinJf4wuz4PqhIwMZIjtS-G3QnoDqGjds',
    authDomain: 'projeto2019-bcb96.firebaseapp.com',
    databaseURL: 'https://projeto2019-bcb96.firebaseio.com',
    projectId: 'projeto2019-bcb96',
    storageBucket: 'projeto2019-bcb96.appspot.com',
    messagingSenderId: '31113503716',
    appId: '1:31113503716:web:a2f6cecb3e4539a1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
